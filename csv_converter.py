import sys, getopt
import csv
import json

#Get Command Line Arguments

class json_converter():
    def main(self,argv):
        input_file = 'D:/Users/Solaiman/Documents/csv_tool/upload_folder/%s'%self.fn
        output_file = 'D:/Users/Solaiman/Documents/csv_tool/csv_jsn.json'
        format = ''
        try:
            opts, args = getopt.getopt(argv,"hi:o:f:",["ifile=","ofile=","format="])
        except getopt.GetoptError:
            print ('csv_json.py -i <path to inputfile> -o <path to outputfile> -f <dump/pretty>')
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print ('csv_json.py -i <path to inputfile> -o <path to outputfile> -f <dump/pretty>')
                sys.exit()
            elif opt in ("-i", "--ifile"):
                input_file = arg
            elif opt in ("-o", "--ofile"):
                output_file = arg
            elif opt in ("-f", "--format"):
                format = arg
        self.read_csv(input_file, output_file, format)
    
    #Read CSV File
    def read_csv(self,file, json_file, format):
        csv_rows = []
        with open(file, 'r',encoding='utf-8') as csvfile:
            dialect = csv.Sniffer().sniff(csvfile.read(), delimiters=';,')
            csvfile.seek(0)
            reader = csv.DictReader(csvfile, dialect=dialect)
            title = reader.fieldnames
            for row in reader:
                csv_rows.extend([{title[i]:row[title[i]] for i in range(len(title))}])
            self.write_json(csv_rows, json_file, format)
    
    #Convert csv data into json and write it
    def write_json(self,data, json_file, format):
        with open(json_file, "w") as f:
            if format == "pretty":
                f.write(json.dumps(data, sort_keys=False, indent=4, separators=(',', ';',': '),encoding="utf-8",ensure_ascii=False))
            else:
                f.write(json.dumps(data))
    def run(self,fn):
        self.fn=fn
        self.main(sys.argv[1:])
        print('klaar')