#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 10:59:15 2018

@author: solaimanelbacha
"""
import os
from flask import Flask, flash, request, redirect, url_for,render_template
from werkzeug.utils import secure_filename
from csv_converter import *
import json


UPLOAD_FOLDER = 'D:/Users/Solaiman/Documents/csv_tool/upload_folder'
ALLOWED_EXTENSIONS = set(['csv'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
opslag = []
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            opslag.append(filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            
            return redirect(url_for('csv_to_json',
                                    filename=filename))
    return render_template('index.html')
@app.route('/uploads/')    
def csv_to_json():
    fn = ".".join(opslag)
    print(fn)
    r_a=json_converter() 
    r_a.run(fn)
    with open('D:/Users/Solaiman/Documents/csv_tool/csv_jsn.json') as f:
        data = json.load(f)
    del opslag[:]
    return render_template('producten.html',data=data)

if __name__ == '__main__':
   app.secret_key = 'super secret key'
   app.config['SESSION_TYPE'] = 'filesystem'
   app.run(debug = True)    